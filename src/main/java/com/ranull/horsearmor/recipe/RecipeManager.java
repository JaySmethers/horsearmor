package com.ranull.horsearmor.recipe;

import com.ranull.horsearmor.HorseArmor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;

import java.util.Iterator;

public class RecipeManager {
    private HorseArmor plugin;

    public RecipeManager(HorseArmor plugin) {
        this.plugin = plugin;
    }

    public void addRecipes() {
        ironArmorRecipe();
        goldenArmorRecipe();
        diamondArmorRecipe();
        saddleRecipe();
        nametagRecipe();
    }

    public void removeRecipes() {
        Iterator<Recipe> recipes = plugin.getServer().recipeIterator();
        Recipe recipe;
        while (recipes.hasNext()) {
            recipe = recipes.next();
            if (recipe == null) {
                continue;
            }
            if (recipe.getResult().getType().equals(Material.IRON_HORSE_ARMOR)
                    || recipe.getResult().getType().equals(Material.GOLDEN_HORSE_ARMOR)
                    || recipe.getResult().getType().equals(Material.DIAMOND_HORSE_ARMOR)
                    || recipe.getResult().getType().equals(Material.NAME_TAG)
                    || recipe.getResult().getType().equals(Material.SADDLE)) {
                recipes.remove();
            }
        }
    }

    public void ironArmorRecipe() {
        ItemStack item = new ItemStack(Material.IRON_HORSE_ARMOR);
        NamespacedKey key = new NamespacedKey(plugin, "iron_horse_armor_recipe");
        ShapedRecipe recipe = new ShapedRecipe(key, item);
        recipe.shape("I I", "III", "I I");
        recipe.setIngredient('I', Material.IRON_INGOT);
        plugin.getServer().addRecipe(recipe);
    }

    public void goldenArmorRecipe() {
        ItemStack item = new ItemStack(Material.GOLDEN_HORSE_ARMOR);
        NamespacedKey key = new NamespacedKey(plugin, "golden_horse_armor_recipe");
        ShapedRecipe recipe = new ShapedRecipe(key, item);
        recipe.shape("G G", "GGG", "G G");
        recipe.setIngredient('G', Material.GOLD_INGOT);
        plugin.getServer().addRecipe(recipe);
    }

    public void diamondArmorRecipe() {
        ItemStack item = new ItemStack(Material.DIAMOND_HORSE_ARMOR);
        NamespacedKey key = new NamespacedKey(plugin, "diamond_horse_armor_recipe");
        ShapedRecipe recipe = new ShapedRecipe(key, item);
        recipe.shape("D D", "DDD", "D D");
        recipe.setIngredient('D', Material.DIAMOND);
        plugin.getServer().addRecipe(recipe);
    }

    public void saddleRecipe() {
        ItemStack item = new ItemStack(Material.SADDLE);
        NamespacedKey key = new NamespacedKey(plugin, "saddle_recipe");
        ShapedRecipe recipe = new ShapedRecipe(key, item);
        recipe.shape("LLL", "LIL", "I I");
        recipe.setIngredient('L', Material.LEATHER);
        recipe.setIngredient('I', Material.IRON_INGOT);
        plugin.getServer().addRecipe(recipe);
    }

    public void nametagRecipe() {
        ItemStack item = new ItemStack(Material.NAME_TAG);
        NamespacedKey key = new NamespacedKey(plugin, "nametag_recipe");
        ShapelessRecipe recipe = new ShapelessRecipe(key, item);
        recipe.addIngredient(Material.IRON_INGOT);
        recipe.addIngredient(Material.PAPER);
        plugin.getServer().addRecipe(recipe);
    }
}
